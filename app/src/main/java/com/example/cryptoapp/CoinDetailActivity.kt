package com.example.cryptoapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.cryptoapp.databinding.ActivityCoinDetailBinding
import com.squareup.picasso.Picasso

private lateinit var viewModel: CoinViewModel
private lateinit var coinDetailBinding: ActivityCoinDetailBinding

class CoinDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coinDetailBinding = ActivityCoinDetailBinding.inflate(layoutInflater)
        val coinDetailView = coinDetailBinding.root
        setContentView(coinDetailView)
        if (!intent.hasExtra(EXTRA_FROM_SYMBOL)) {
            finish()
            return
        }
        val fromSymbol = intent.getStringExtra(EXTRA_FROM_SYMBOL)
        viewModel = ViewModelProviders.of(this)[CoinViewModel::class.java]
        if (fromSymbol != null) {
            viewModel.getDetailInfo(fromSymbol).observe(this, Observer {
                with(coinDetailBinding) {
                    tvPrice.text = it.pRICE
                    tvMinPrice.text = it.lOWDAY
                    tvMaxPrice.text = it.hIGHDAY
                    tvLastMarket.text = it.lASTMARKET
                    tvLastUpdate.text = it.getFormattedTime()
                    tvFromSymbol.text = it.fROMSYMBOL
                    tvToSymbol.text = it.tOSYMBOL
                    Picasso.get().load(it.getFullImageUrl()).into(ivLogoCoin)
                }
            })
        }
    }

    companion object {
        private const val EXTRA_FROM_SYMBOL: String = "fSym"

        fun newIntent(context: Context, fromSymbol: String): Intent {
            val intent = Intent(context, CoinDetailActivity::class.java)
            intent.putExtra(EXTRA_FROM_SYMBOL, fromSymbol)
            return intent
        }
    }
}